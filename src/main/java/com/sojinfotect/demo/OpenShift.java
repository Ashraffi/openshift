package com.sojinfotect.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/")
public class OpenShift {
    @GetMapping("getMessage")
    public String getMessage(){
        return  "Hello World";
    }
}
